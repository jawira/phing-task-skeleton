# Phing Task Skeleton

**Dummy task to use as a base to create a new task**

<details>
    <summary>ℹ️ To create a new project</summary>
    <ol>
    <li><code>$ composer create-project jawira/phing-task-skeleton</code></li>
    <li>Rename <code>src/DummyTask.php</code> including namespace and class</li>
    <li>Update <code>composer.json</code>, do not forget to update <code>extra</code> and <code>autoload</code></li>
    </ol>
</details>

<!-- https://poser.pugx.org/ -->

## Usage

| Name              | Type      | Description               | Default       | Required  |
|-------------------|-----------|---------------------------|---------------|-----------|
| `message`         | _String_  | Nice message to display   | `DummyTask`   | No        |
| `source`          | _String_  | Path to file              | n/a           | No        |
| `showLocation`    | _Boolean_ | Show task location        | `false`       | No        |

```xml
<dummy message="Welcome to the jungle"
       source="./README.md" 
       showLocation="yes"/>
```

## License

This library is licensed under the [MIT license](LICENSE.md).
