<?php

namespace Jawira\PhingTaskSkeleton;

use BuildException;
use PhingFile;
use Project;
use Task;

/**
 * @link https://www.phing.info/guide/chunkhtml/ch06s06.html
 */
class DummyTask extends Task
{
    protected string     $message;
    protected ?PhingFile $source;
    protected bool       $showLocation;

    /**
     * This is executed before setters
     */
    public function init()
    {
        $this->message      = 'DummyTask';
        $this->source       = null;
        $this->showLocation = false;
    }

    public function setSource(PhingFile $source)
    {
        if (!$source->isFile()) {
            // \BuildException must be used in your task
            throw new BuildException('If set, source must be a file.');
        }

        $this->source = $source;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * Only "true", "yes" and "1" are _true_, anything else is _false_.
     */
    public function setShowLocation(bool $showLocation)
    {
        $this->showLocation = $showLocation;
    }


    /**
     * The Task logic goes here
     */
    public function main()
    {
        $this->log($this->message, Project::MSG_INFO);

        if ($this->source) {
            $this->log($this->source, Project::MSG_INFO);
        }

        if ($this->showLocation) {
            $this->log($this->getLocation(), Project::MSG_INFO);
        }
    }

}
